#!/bin/bash

manager=$1
project=$2
brand=$3

# Initialize the Wails project with Svelte template
wails init -n $project -t svelte

# Navigate to the project directory
cd $project

# Update wails.json to use the specified package manager
sed -i "s|npm|$manager|g" wails.json

# Add wailsjsdir entry to wails.json
sed -i 's|"auto",|"auto",\n  "wailsjsdir": "./frontend/src/lib",|' wails.json

# Update main.go to change the build path
sed -i "s|all:frontend/dist|all:frontend/build|" main.go

# If brand is specified, make additional changes
if [[ -n $brand ]]; then
    mv frontend/src/App.svelte +page.svelte
    sed -i "s|'./assets|'\$lib/assets|" +page.svelte
    sed -i "s|'../wails|'\$lib/wails|" +page.svelte
    mv frontend/src/assets .
fi

# Remove the existing frontend directory
rm -r frontend

# Create a new Svelte project in the frontend directory
$manager create svelte@latest frontend

# If brand is specified, move and adjust assets
if [[ -n $brand ]]; then
    mv +page.svelte frontend/src/routes/+page.svelte
    mkdir frontend/src/lib
    mv assets frontend/src/lib/
fi

# Navigate to the frontend directory
cd frontend

# Install dependencies
$manager i

# Uninstall the default Svelte adapter
$manager uninstall @sveltejs/adapter-auto

# Install the static adapter
$manager i -D @sveltejs/adapter-static

# Add prerender and ssr settings to +layout.ts
echo -e "export const prerender = true\nexport const ssr = false" > src/routes/+layout.ts

# Update svelte.config.js to use the static adapter
sed -i "s|-auto';|-static';|" svelte.config.js

# Navigate back to the project root
cd ..

# Start the Wails development server
wails dev
